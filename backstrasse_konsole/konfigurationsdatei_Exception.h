#pragma once
#include <iostream>
using namespace std;


class konfigurationsdatei_exception : public exception
{
	string msg;
public:
	konfigurationsdatei_exception();
	konfigurationsdatei_exception(string dateiname);
	const char * what() const noexcept;
	~konfigurationsdatei_exception();
};

