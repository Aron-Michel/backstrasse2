#pragma once
#include <iostream>
#include <string>
#include <map>
using namespace std;

class konfigurationsdatei
{
private:
	string dateiname;
	map<string, string> zutaten_teig;
public:
	void write_file(string);
	void read_file();
	void setDateiname(string);
	map<string, string> getZutatenTeig();
	konfigurationsdatei();
	~konfigurationsdatei();
};

