#include "stdafx.h"
#include "konfigurationsdatei_Exception.h"


konfigurationsdatei_exception::konfigurationsdatei_exception() :
	msg{ "Eine Datei konnte nicht geoeffnet werden. Das Programm wird beendet." }
{
}

konfigurationsdatei_exception::konfigurationsdatei_exception(string dateiname) :
	msg{ "Die Datei " + dateiname + " konnte nicht geoeffnet werden. Das Programm wird beendet." }

{
}

const char * konfigurationsdatei_exception::what() const noexcept
{
	return msg.c_str();
}


konfigurationsdatei_exception::~konfigurationsdatei_exception()
{
}
