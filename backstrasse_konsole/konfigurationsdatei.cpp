#include "stdafx.h"
#include <fstream>
#include "konfigurationsdatei.h"


void konfigurationsdatei::write_file(string)
{
}

void konfigurationsdatei::read_file()
{
	fstream  f;
	char cstring[256];
	string s1;
	//map<string,string> m;
	int i{ 0 }, j{ 0 };

	try
	{
		f.open(dateiname, ios_base::in);
		if (!f.is_open())
		{
			throw exception("Artikeldatei zum Lesen nicht offen.");
		}

		while (f.getline(cstring, 100))
		{
			if (i >= 4 && i <= 5)
			{
				j = 0;
				while (cstring[j]!='\0')
				{
					s1.append(1, cstring[j]);
					if (cstring[j] != '|')
					{
						s1.erase(j,1);
						zutaten_teig.at(s1) = "";
					}
					j++;
				}
			}
			
			i++;
		}
	}
	catch (exception &e)
	{
		cout << e.what() << endl;
	}
}

void konfigurationsdatei::setDateiname(string s)
{
	dateiname = s;
}

map<string, string> konfigurationsdatei::getZutatenTeig()
{
	return zutaten_teig;
}

konfigurationsdatei::konfigurationsdatei()
{
}


konfigurationsdatei::~konfigurationsdatei()
{
}
