#pragma once
#include <iostream>
#include <string>
#include <map>
using namespace std;
#include "ofen.h"
#include "rezeptur.h"
#include "verzieren.h"


class backstrasse
{
private:
	double geschwindigkeit;
	map<string,string> zutaten_teig;
	map<string, string> zutaten_belag;
	map<string, string> zutaten_glasur;
	ofen * p_ofen = nullptr;
	rezeptur * p_rezeptur = nullptr;
	verzieren * p_verzieren = nullptr;
public:
	double getGeschwindigkeit();
	void setGeschwindigkeit(double geschwindigkeit);
	backstrasse();
	backstrasse(double geschw);
	~backstrasse();
};

