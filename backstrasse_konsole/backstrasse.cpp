#include "stdafx.h"
#include "backstrasse.h"


double backstrasse::getGeschwindigkeit()
{
	return this->geschwindigkeit;
}

void backstrasse::setGeschwindigkeit(double geschwindigkeit)
{
	this->geschwindigkeit = geschwindigkeit;
}

backstrasse::backstrasse()
{
	p_ofen = new ofen();
	p_rezeptur = new rezeptur();
}

backstrasse::backstrasse(double geschw) : geschwindigkeit{ geschw }
{
}


backstrasse::~backstrasse()
{
	delete (p_ofen);
	p_ofen = nullptr;
	delete (p_rezeptur);
	p_rezeptur = nullptr;
	if (p_verzieren)
	{
		delete (p_verzieren);
		p_verzieren = nullptr;
	}
}
