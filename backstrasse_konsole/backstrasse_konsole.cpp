// backstrasse_konsole.cpp: Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include <iostream>
#include "konfigurationsdatei_Exception.h"
#include "backstrasse.h"

using namespace std;


int main()
{
	try
	{
		backstrasse * p_backstrasse = new backstrasse(50);
		cout << "Geschwindigkeit des Laufbandes: " 
			<< p_backstrasse->getGeschwindigkeit() << endl;
		p_backstrasse->setGeschwindigkeit(100);
		cout << "Geschwindigkeit des Laufbandes: " 
			<< p_backstrasse->getGeschwindigkeit() << endl;
		delete (p_backstrasse);
		p_backstrasse = nullptr;
		//throw konfigurationsdatei_exception("konfig.txt");
		return 0;

	}
	catch (exception &e)
	{
		cout << "Exception in backstrasse_konsole.cpp " << endl;
		cout << e.what() << endl;
		return 1;
	}
}

