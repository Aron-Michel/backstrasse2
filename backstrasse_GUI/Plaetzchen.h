#pragma once
ref class Plaetzchen
{
private:
	double backzeit;
	double temperatur;
public:
	Plaetzchen();
	Plaetzchen(double backzeit, double temperatur);

	double getBackzeit();
	void setBackzeit(double backzeit);
	double getTemperatur();
	void setTemperatur(double temperatur);

	~Plaetzchen();
};

