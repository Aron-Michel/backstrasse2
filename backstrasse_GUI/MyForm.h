#pragma once
#include "Plaetzchen.h"

namespace backstrasseGUI {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button_Export;
	protected:

	protected:
	private: System::Windows::Forms::Button^  button_Ende;

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button_Export = (gcnew System::Windows::Forms::Button());
			this->button_Ende = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// button_Export
			// 
			this->button_Export->Location = System::Drawing::Point(1155, 491);
			this->button_Export->Name = L"button_Export";
			this->button_Export->Size = System::Drawing::Size(75, 23);
			this->button_Export->TabIndex = 0;
			this->button_Export->Text = L"E&xportiern";
			this->button_Export->UseVisualStyleBackColor = true;
			this->button_Export->Click += gcnew System::EventHandler(this, &MyForm::button_Export_Click);
			// 
			// button_Ende
			// 
			this->button_Ende->Location = System::Drawing::Point(1155, 525);
			this->button_Ende->Name = L"button_Ende";
			this->button_Ende->Size = System::Drawing::Size(75, 23);
			this->button_Ende->TabIndex = 1;
			this->button_Ende->Text = L"&Ende";
			this->button_Ende->UseVisualStyleBackColor = true;
			this->button_Ende->Click += gcnew System::EventHandler(this, &MyForm::button_Ende_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1299, 599);
			this->Controls->Add(this->button_Ende);
			this->Controls->Add(this->button_Export);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button_Ende_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void button_Export_Click(System::Object^  sender, System::EventArgs^  e);
	};
}
