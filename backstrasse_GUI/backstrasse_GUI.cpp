#include "MyForm.h"

using namespace backstrasseGUI;

[STAThreadAttribute]
int main()
{
	MyForm ^ p_form = gcnew MyForm();
	p_form->ShowDialog();
	return 0;
}