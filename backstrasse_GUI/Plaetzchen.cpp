#include "Plaetzchen.h"



Plaetzchen::Plaetzchen()
{
	temperatur = 0.0;
	backzeit = 0.0;
}

Plaetzchen::Plaetzchen(double backzeit, double temperatur)
{
	temperatur = temperatur;
	backzeit = backzeit;
}

double Plaetzchen::getBackzeit()
{
	return this->backzeit;
}

void Plaetzchen::setBackzeit(double backzeit)
{
	this->backzeit = backzeit;
}

double Plaetzchen::getTemperatur()
{
	return this->temperatur;
}

void Plaetzchen::setTemperatur(double temperatur)
{
	this->temperatur = temperatur;
}

Plaetzchen::~Plaetzchen()
{

}
